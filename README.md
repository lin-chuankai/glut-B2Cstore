# 桂工杂货铺

#### 项目介绍
本项目是基于springboot的B2C微服务商城系统，前端采用vue框架，后端采用springboot框架，数据库使用MySQL，最后部署到云服务器上。

#### 项目学习
本项目是本人基于尚硅谷电商毕设项目改造而来的（https://www.wolai.com/atguigu/m4z5zhigfZdUSvfTUJvYZM）

#### 开发工具
| 开发工具          | 版本号      |
|---------------|----------|
| JDK           | 17.0.6   |
| Intellij IDEA | 2021.3.3 |
| Maven         | 3.6.3    |
| MySQL         | 8.0.29   |

#### 核心技术栈
| 前端        | 后台         |
|-----------|------------|
| vue       | springboot |
| node      | rabbitMQ   |
| elementUI | ES         |
| LayUI     | Redis      |
| Axios     | Lombok     |

#### 系统部署说明

1.  首先需要自己在idea中部署Maven环境（具体内容百度）
2.  其次需要自己购买一个云服务器，并在项目中修改为自己的IP地址以及相应的数据库
3.  依次启动服务

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
