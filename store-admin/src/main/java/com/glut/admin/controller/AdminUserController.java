package com.glut.admin.controller;

import com.glut.admin.param.AdminUserParam;
import com.glut.admin.pojo.AdminUser;
import com.glut.admin.service.AdminUserService;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/11
 **/
@RestController
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;

    @PostMapping("/user/login")
    public R login(@Validated AdminUserParam adminUserParam, BindingResult result, HttpSession session){
        if(result.hasErrors()){
            return R.fail("核心参数，登录失败");
        }

        String captcha = (String) session.getAttribute("captcha");
        if(!adminUserParam.getVerCode().equalsIgnoreCase(captcha)){
            return R.fail("验证码错误");
        }
         AdminUser user=adminUserService.login(adminUserParam);
        if(user==null){
            return R.fail("登录失败，账号或者密码为空");
        }

        session.setAttribute("userInfo",user);
        return R.ok("登录成功");
    }

    @GetMapping("user/logout")
    public R logout(HttpSession session){
        session.invalidate();

        return R.ok("退出登录成功");
    }
}
