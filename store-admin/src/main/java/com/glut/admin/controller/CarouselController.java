package com.glut.admin.controller;

import com.glut.admin.service.CarouselService;
import com.glut.admin.utils.AliyunOSSUtils;
import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/24
 **/
@RestController
@RequestMapping("/carousel")
public class CarouselController {

    @Autowired
    private CarouselService carouselService;

    @Autowired
    private AliyunOSSUtils aliyunOSSUtils;

    @GetMapping("list")
    public R adminList(){

        return carouselService.search();
    }

    @PostMapping("upload")
    public R adminUpload(@RequestParam("img") MultipartFile img) throws Exception {

        String filename=img.getOriginalFilename();
        filename= UUID.randomUUID().toString().replaceAll("-","")+filename;
        String contentType = img.getContentType();
        byte[] content = img.getBytes();

        int hours=1000;
        String url= aliyunOSSUtils.uploadImage(filename,content,contentType,hours
        );
        System.out.println("url="+url);
        return R.ok("图片上传成功",url);
    }



    @PostMapping("save")
    public R adminSave(CarouselSaveParam carouselSaveParam){

        return carouselService.save(carouselSaveParam);
    }


    @PostMapping("remove")
    public R adminRemove(Integer carouselId){

        return carouselService.remove(carouselId);
    }
}
