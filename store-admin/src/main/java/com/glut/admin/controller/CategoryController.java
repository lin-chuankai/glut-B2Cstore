package com.glut.admin.controller;


import com.glut.admin.service.CategoryService;
import com.glut.param.PageParam;
import com.glut.pojo.Category;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/12
 **/
@RestController
@RequestMapping("category")
public class CategoryController {


    @Autowired
    private CategoryService categoryService;
    @GetMapping("list")
    public R pageList(PageParam pageParam){


        return categoryService.pageList(pageParam);

    }

    @PostMapping("save")
    public R pageSave(Category category){


        return categoryService.save(category);

    }

    @PostMapping("remove")
    public R remove(Integer categoryId){


        return categoryService.remove(categoryId);

    }

    @PostMapping("update")
    public R update(Category category){


        return categoryService.update(category);

    }
}
