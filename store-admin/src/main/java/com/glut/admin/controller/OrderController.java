package com.glut.admin.controller;

import com.glut.admin.service.OrderService;
import com.glut.param.PageParam;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/15
 **/
@RestController
@RequestMapping("order")
public class OrderController {


    @Autowired
    private OrderService orderService;
    @GetMapping("/list")
    public R list(PageParam pageParam){


        return orderService.list(pageParam);
    }
}
