package com.glut.admin.controller;

import com.glut.admin.service.UserService;
import com.glut.param.CartListParam;
import com.glut.param.PageParam;
import com.glut.pojo.User;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/12
 **/
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("list")
    public R userList(PageParam pageParam){
        return userService.userList(pageParam);
    }


    @PostMapping("remove")
    public R userList(CartListParam pageParam){
        return userService.userRemove(pageParam);
    }

    @PostMapping("update")
    public R update(User user){
        return userService.userUpdate(user);
    }

    @PostMapping("save")
    public R save(User user){
        return userService.save(user);
    }
}
