package com.glut.admin.inteceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description: TODO 进来的都要拦截 检查session是否有数据  有 放行  没有 转到登录界面
 * @Author: lck
 * @Date: 2023/3/11
 **/
@Component
public class LoginProtectInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object userInfo = request.getSession().getAttribute("userInfo");

        if (userInfo != null) {
            return true;
        }else{

            response.sendRedirect(request.getContextPath()+"/index.html");
            return false;
        }


    }
}
