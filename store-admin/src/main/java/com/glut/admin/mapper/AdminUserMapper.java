package com.glut.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.admin.pojo.AdminUser;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/11
 **/
public interface AdminUserMapper extends BaseMapper<AdminUser> {
}
