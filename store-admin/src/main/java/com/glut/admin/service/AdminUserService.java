package com.glut.admin.service;

import com.glut.admin.param.AdminUserParam;
import com.glut.admin.pojo.AdminUser;

public interface AdminUserService {
    /**
     * 登录业务方法
     * @param adminUserParam
     * @return
     */
    AdminUser login(AdminUserParam adminUserParam);
}
