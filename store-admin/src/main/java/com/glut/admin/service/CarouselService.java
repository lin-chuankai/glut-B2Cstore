package com.glut.admin.service;

import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;

public interface CarouselService {

    R search();

    R save(CarouselSaveParam carouselSaveParam);

    R remove(Integer carouselId);
}
