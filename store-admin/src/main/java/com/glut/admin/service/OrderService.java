package com.glut.admin.service;

import com.glut.param.PageParam;
import com.glut.utils.R;

public interface OrderService {
    /**
     * 查询订单数据
     * @param pageParam
     * @return
     */
    R list(PageParam pageParam);
}
