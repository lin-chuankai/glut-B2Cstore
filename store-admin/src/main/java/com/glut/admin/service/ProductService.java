package com.glut.admin.service;

import com.glut.param.ProductSaveParam;
import com.glut.param.ProductSearchParam;
import com.glut.pojo.Product;
import com.glut.utils.R;

public interface ProductService {
    /**
     * 全部商品查询和搜索查询
     * @param productSearchParam
     * @return
     */
    R search(ProductSearchParam productSearchParam);

    /**
     * 进行商品数据保存
     * @param productSaveParam
     * @return
     */
    R save(ProductSaveParam productSaveParam);

    /**
     * 更新商品数据
     * @param product
     * @return
     */
    R update(Product product);

    /**
     * 商品移除
     * @param productId
     * @return
     */
    R remove(Integer productId);
}
