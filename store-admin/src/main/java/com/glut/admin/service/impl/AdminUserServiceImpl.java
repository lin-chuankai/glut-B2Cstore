package com.glut.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.glut.admin.mapper.AdminUserMapper;
import com.glut.admin.param.AdminUserParam;
import com.glut.admin.pojo.AdminUser;
import com.glut.admin.service.AdminUserService;
import com.glut.constants.UserConstants;
import com.glut.utils.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/11
 **/
@Service
@Slf4j
public class AdminUserServiceImpl implements AdminUserService {

    @Autowired
    private AdminUserMapper adminUserMapper;
    /**
     * 登录业务方法
     *
     * @param adminUserParam
     * @return
     */
    @Override
    public AdminUser login(AdminUserParam adminUserParam) {

        QueryWrapper<AdminUser> queryWrapper =
                new QueryWrapper<>();

        queryWrapper.eq("user_account",adminUserParam.getUserAccount());
        queryWrapper.eq("user_password", MD5Util.encode(adminUserParam.getUserPassword()+ UserConstants.USER_SLAT));
        AdminUser user = adminUserMapper.selectOne(queryWrapper);
        log.info("AdminUserServiceImpl.login业务结束，结果:{}",user);
        return user;

        //结果封装

//        if (adminUser == null) {
//            return R.fail("账号或者密码错误!");
//        }
//
//        R ok = R.ok("用户登录成功!", adminUser);
//        log.info("AdminUserServiceImpl.login业务结束，结果:{}",ok);
//
//        return ok;
    }
}
