package com.glut.admin.service.impl;

import com.glut.admin.service.CarouselService;
import com.glut.clients.CarouselClient;
import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/24
 **/
@Service
@Slf4j
public class CarouselServiceImpl implements CarouselService {
    @Autowired
    CarouselClient carouselClient;

    @Override
    public R search(){
        R search=carouselClient.search();
        log.info("CarouselServiceImpl.search业务结束，结果：{}",search);
        return search;
    }

    @Override
    public R save(CarouselSaveParam carouselSaveParam) {
        R r =carouselClient.adminSave(carouselSaveParam);
        log.info("CarouselServiceImpl.save业务结束，结果：{}", r);
        return r;
    }

    @Override
    public R remove(Integer carouselId) {
        R r = carouselClient.adminRemove(carouselId);
        log.info("CarouselServiceImpl.remove业务结束，结果：{}", r);
        return r;
    }
}
