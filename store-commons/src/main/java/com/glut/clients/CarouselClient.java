package com.glut.clients;

import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "carousel-service")
public interface CarouselClient {
    @PostMapping("/carousel/list")
    R search();

    @PostMapping("/carousel/admin/save")
    R adminSave(CarouselSaveParam carouselSaveParam);

    @PostMapping("/carousel/admin/remove")
    R adminRemove(Integer carouselId);
}
