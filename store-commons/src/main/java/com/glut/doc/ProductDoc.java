package com.glut.doc;

import com.glut.pojo.Product;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@Data
@NoArgsConstructor
public class ProductDoc extends Product {
    /**
     * 商品名称和商品标题和商品描述的综合值.
     */
    private String all;

    public ProductDoc(Product product) {
        super(product.getProductId(),product.getProductName(),product.getCategoryId(),product.getProductTitle()
        ,product.getProductIntro(),product.getProductPicture(),product.getProductPrice(), product.getProductSellingPrice(),
                product.getProductSales(),product.getProductNum());

        this.all=product.getProductName()+product.getProductTitle()+product.getProductIntro();
    }
}
