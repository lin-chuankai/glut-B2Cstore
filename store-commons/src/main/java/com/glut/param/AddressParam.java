package com.glut.param;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.glut.pojo.Address;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/10
 **/
@Data
public class AddressParam {
    @NotNull
    @JsonProperty("user_id")
    private Integer userId;

    private Address add;
}
