package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/5
 **/
@Data
public class AddressRemoveParam {

    @NotNull
    private Integer id;
}
