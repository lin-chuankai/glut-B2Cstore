package com.glut.param;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/9
 **/
@Data
public class CartSaveParam {
    @JsonProperty("product_id")
    @NotNull
    private  Integer productId;
    @JsonProperty("user_id")
    @NotNull
    private Integer userId;
}
