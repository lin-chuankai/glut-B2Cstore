package com.glut.param;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.glut.vo.CartVo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/10
 **/
@Data
public class OrderParam implements Serializable {

    public static final Long serialVersionUID = 1L;
    @JsonProperty("user_id")
    private Integer userId;
    private List<CartVo>products;
}
