package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/9
 **/
@Data
public class ProductCollectParam {

    @NotEmpty
    private List<Integer> productIds;
}
