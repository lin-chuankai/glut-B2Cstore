package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/7
 **/
@Data
public class ProductHotParam {
    @NotEmpty
    private List<String> categoryName;

}
