package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@Data
public class ProductIdParam {

    @NotNull
    private Integer productID;
}
