package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@Data
public class ProductIdsParam extends PageParam{

    @NotNull
    private List<Integer> categoryID;

}
