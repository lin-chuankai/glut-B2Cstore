package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/7
 **/
@Data
public class ProductPromoParam {

    @NotBlank
    private String categoryName;
}
