package com.glut.param;

import com.glut.pojo.Product;
import lombok.Data;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/13
 **/
@Data
public class ProductSaveParam extends Product {
    /**
     * 保存商品详情图片地址 图片之间使用+拼接
     */
    private String pictures;
}
