package com.glut.param;

import lombok.Data;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@Data
public class ProductSearchParam extends PageParam{

    private String search;

}
