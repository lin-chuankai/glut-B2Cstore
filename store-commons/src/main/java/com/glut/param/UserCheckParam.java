package com.glut.param;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/5
 * TODO:要使用jsr 303的注解进行参数校验
 * @Blank 字符串 不能为null 和空字符串
 * @NotNull 字符串 不能为null
 * @NotEmpty 集合类型 集合长度不能为0
 **/
@Data
public class UserCheckParam {
    @NotBlank
    private String userName;
}
