package com.glut.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/5
 **/
@Data
@TableName("user")
public class User implements Serializable {
    public static final Long serialVersionUID=1L;

    @JsonProperty("user_id")//json的注解，用于进行属性格式化
    @TableId(type = IdType.AUTO)
    private Integer userId;

    @Length(min = 6)
    private String userName;

   // @JsonIgnore //忽略属性 不生成Json 不接受json数据
    @JsonInclude(JsonInclude.Include.NON_NULL)//不影响接受json
    @NotBlank
    private String password;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotBlank
    private String userPhonenumber;
}
