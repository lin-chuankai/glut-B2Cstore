package com.glut.to;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/10
 **/
@Data
public class OrderToProduct implements Serializable {

    public static final Long serialVersionUID = 1L;
    //商品id
    private Integer productId;
    //购买数量
    private Integer Num;
}
