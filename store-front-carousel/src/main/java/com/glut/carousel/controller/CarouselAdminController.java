package com.glut.carousel.controller;

import com.glut.carousel.service.CarouselService;
import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author: lck
 *
 * @Date: 2023/3/24
 **/
@RestController
@RequestMapping("carousel")
public class CarouselAdminController {


    @Autowired
    private CarouselService carouselService;

    @PostMapping("/admin/save")
    public R adminSave(@RequestBody CarouselSaveParam carouselSaveParam){

        return carouselService.adminSave(carouselSaveParam);
    }



    @PostMapping("/admin/remove")
    public R adminRemove(@RequestBody Integer carouselId){

        return carouselService.adminRemove(carouselId);
    }
}
