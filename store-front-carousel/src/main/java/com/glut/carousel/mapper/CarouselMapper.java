package com.glut.carousel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Carousel;
import org.springframework.stereotype.Repository;

@Repository
public interface CarouselMapper extends BaseMapper<Carousel> {
}
