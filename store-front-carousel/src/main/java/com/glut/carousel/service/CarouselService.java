package com.glut.carousel.service;


import com.glut.param.CarouselSaveParam;
import com.glut.utils.R;

public interface CarouselService {

    /**
     * 查询优先级最高的轮播图数据
     * @return
     */
    R list1();


    R adminSave(CarouselSaveParam carouselSaveParam);


    R adminRemove(Integer carouselId);
}
