package com.glut.carousel.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.glut.carousel.mapper.CarouselMapper;
import com.glut.carousel.service.CarouselService;
import com.glut.param.CarouselSaveParam;
import com.glut.pojo.Carousel;
import com.glut.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/6
 **/

@Service
@Slf4j
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements CarouselService {

    @Autowired
    private CarouselMapper carouselMapper;
    /**
     * 查询优先级最高的六条轮播图数据
     *   按照优先级查询数据库数据
     *   我们使用stream流,进行内存数据切割,保留6条数据!
     * @return
     */

    //@Cacheable(value="list.carousel",key = "#root.methodName" ,cacheManager = "cacheManagerDay")
    @Override
    public R list1() {

        QueryWrapper<Carousel> carouselQueryWrapper = new QueryWrapper<>();
        carouselQueryWrapper.orderByDesc("priority");

        List<Carousel> list = carouselMapper.selectList(carouselQueryWrapper);
        //jdk 1.8 stream
        List<Carousel> collect = list.stream().limit(6).collect(Collectors.toList());
        Long total=collect.parallelStream().count();
        R ok = R.ok(null,collect,total);

        log.info("CarouselServiceImpl.list业务结束，结果:{}",ok);
        return ok;
    }

    @Override
    public R adminSave(CarouselSaveParam carouselSaveParam) {

        Carousel carousel = new Carousel();
        //参数赋值
        BeanUtils.copyProperties(carouselSaveParam, carousel);
        //商品数据保存
        int rows = carouselMapper.insert(carousel);
        log.info("CarouselServiceImpl.adminSave业务结束，结果:{}", rows);
        return R.ok("轮播图数据添加成功!");
    }

    @Override
    public R adminRemove(Integer carouselId) {
        //删除轮播图数据
        carouselMapper.deleteById(carouselId);

        R ok = R.ok("轮播图数据删除成功!");
        return ok;
    }
}
