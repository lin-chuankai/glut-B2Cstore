package com.glut.cart.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Cart;
import org.springframework.stereotype.Repository;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/9
 **/
@Repository
public interface CartMapper extends BaseMapper<Cart> {
}
