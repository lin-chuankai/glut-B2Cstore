package com.glut.category.controller;

import com.glut.category.service.CategoryService;
import com.glut.param.PageParam;
import com.glut.pojo.Category;
import com.glut.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/12
 **/
@RestController
@RequestMapping("/category")
public class CategoryAdminController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("admin/list")
    public R listPage(@RequestBody PageParam pageParam){

        return categoryService.listPage(pageParam);
    }
    @PostMapping("admin/save")
    public R adminSave(@RequestBody Category category){

        return categoryService.adminSave(category);
    }

    @PostMapping("admin/remove")
    public R adminRemove(@RequestBody Integer categoryId){

        return categoryService.adminRemove(categoryId);
    }
    @PostMapping("admin/update")
    public R adminUpdate(@RequestBody Category category){

        return categoryService.adminUpdate(category);
    }
}
