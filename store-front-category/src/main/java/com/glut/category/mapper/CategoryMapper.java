package com.glut.category.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Category;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/7
 **/
public interface CategoryMapper extends BaseMapper<Category> {
}
