package com.glut.collect.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Collect;

public interface CollectMapper extends BaseMapper<Collect> {
}
