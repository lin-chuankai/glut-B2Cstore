package com.glut.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Order;
import com.glut.vo.AdminOrderVo;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/10
 **/
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 查询后台管理的数据方法
     * @param offset
     * @param pageSize
     * @return
     */
    List<AdminOrderVo> selectAdminOrder(@Param("offset")int offset, @Param("pageSize") int pageSize);
}
