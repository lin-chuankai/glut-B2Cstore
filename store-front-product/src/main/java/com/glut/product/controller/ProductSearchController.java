package com.glut.product.controller;

import com.glut.product.service.ProductService;
import com.glut.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@RestController
@RequestMapping("product")
public class ProductSearchController {
    @Autowired
    private ProductService productService;


    @GetMapping("/list")
    public List<Product> allList(){
        return productService.allList();

    }
}
