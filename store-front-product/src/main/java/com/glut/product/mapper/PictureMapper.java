package com.glut.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Picture;

public interface PictureMapper extends BaseMapper<Picture> {
}
