package com.glut.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Product;

public interface ProductMapper extends BaseMapper<Product> {
}
