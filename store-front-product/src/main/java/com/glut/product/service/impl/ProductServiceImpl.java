package com.glut.product.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.glut.clients.*;
import com.glut.param.ProductSaveParam;
import com.glut.product.mapper.PictureMapper;
import com.glut.product.mapper.ProductMapper;
import com.glut.product.service.ProductService;
import com.glut.param.ProductHotParam;
import com.glut.param.ProductIdsParam;
import com.glut.param.ProductSearchParam;
import com.glut.pojo.Picture;
import com.glut.pojo.Product;
import com.glut.to.OrderToProduct;
import com.glut.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/7
 */
@Service
@Slf4j
public class ProductServiceImpl extends ServiceImpl<ProductMapper,Product> implements ProductService {

    @Autowired
    private CategoryClient categoryClient;

    @Autowired
    private SearchClient searchClient;

    @Autowired
    private OrderClient orderClient;

    @Autowired
    private CartClient cartClient;

    @Autowired
    private CollectClient collectClient;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private PictureMapper pictureMapper;
    /**
     * 根据单类别名称查询热门商品
     *1.根据类别名称，调用feign客户端访问类别服务获取类别的数据
     * 2.成功 继续根据类别id查询商品数据 【热门 销售量倒序 查询7】
     * 3.结果封装即可
     * @param categoryName 类别名称
     * @return r
     */
    @Cacheable(value = "list.product",key="#categoryName",cacheManager = "cacheManagerDay")
    @Override
    public R promo(String categoryName) {


        R r = categoryClient.byName(categoryName);
        log.info("-----------------------------------"+categoryName);
       if (r.getCode().equals(R.FAIL_CODE)){
            log.info("ProductServiceImpl.promo业务结束，结果：{}","类别查询失败");
            return r;
        }
       LinkedHashMap<String,Object>map = (LinkedHashMap<String, Object>) r.getData();
       Integer categoryId=(Integer)map.get("category_id");

       // Integer categoryId=(Integer)map.get("categoryId");

        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("category_id",categoryId);
        queryWrapper.orderByDesc("product_sales");

        IPage<Product>page=new Page<>(1,7);

        page = productMapper.selectPage(page, queryWrapper);
        List<Product> productList = page.getRecords();

        long total = page.getTotal();

        log.info("ProductServiceImpl.promo业务结束，结果：{}",productList);
        return R.ok("数据查询成功",productList);
    }

    /**
     * 多类别热门商品查询 根据类别名称集合
     *1.调用类别服务
     * 2.类别集合id查询商品
     * 3.结果集封装
     * @param productHotParam 类别名称集合
     * @return r
     */
    @Cacheable(value = "list.product",key = "#productHotParam.categoryName")
    @Override
    public R hots(ProductHotParam productHotParam) {
        R r=categoryClient.hots(productHotParam);

        if(r.getCode().equals(R.FAIL_CODE)){
            log.info("ProductServiceImpl.hots业务结束，结果：{}",r.getMsg());
            return r;
        }
        List<Object> ids = (List<Object>) r.getData();
        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        queryWrapper.in("category_id",ids);
        queryWrapper.orderByDesc("product_sales");
        IPage<Product>page=new Page<>(1,7);
        page=productMapper.selectPage(page,queryWrapper);

        List<Product> records = page.getRecords();
        R ok = R.ok("多类别热门商品查询成功", records);
        log.info("ProductServiceImpl.hots业务结束，结果：{}",ok);


        return ok;
    }

    /**
     * 查询类别商品集合
     *
     * @return
     */
    @Override
    public R clist() {
        R r = categoryClient.list();
        log.info("ProductServiceImpl.clist业务结束，结果：{}",r);

        return r;
    }

    /**
     * 通用性业务
     * 传入了类别id，根据id查询并且分页
     * 没有传入，查询全部
     *
     * @param productIdsParam
     * @return
     */
    @Cacheable(value = "list.product",key = "#productIdsParam.categoryID+'-'+#productIdsParam.currentPage+'-'+#productIdsParam.pageSize")
    @Override
    public R byCategory(ProductIdsParam productIdsParam) {
        List<Integer> categoryID = productIdsParam.getCategoryID();

        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        if (!categoryID.isEmpty()) {
            queryWrapper.in("category_id",categoryID);
        }
        IPage<Product>page=new Page<>(productIdsParam.getCurrentPage(),productIdsParam.getPageSize());
        page=productMapper.selectPage(page,queryWrapper);

        R ok = R.ok("查询成功", page.getRecords(), page.getTotal());
        log.info("ProductServiceImpl.byCategory业务结束，结果：{}",ok);
        return ok;
    }

    /**
     * 根据商品id查询商品详情信息
     *
     * @param productID
     * @return
     */
    @Cacheable(value = "list.product",key = "#productID")
    @Override
    public R detail(Integer productID) {

        Product product = productMapper.selectById(productID);
        R ok = R.ok(product);
        log.info("ProductServiceImpl.detail业务结束，结果：{}",ok);
        return ok;
    }

    /**
     * 查询商品对应的图片详情集合
     *
     * @param productID
     * @return
     */
    @Cacheable(value = "picture",key = "#productID")
    @Override
    public R pictures(Integer productID) {

        QueryWrapper<Picture>queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("product_id",productID);
        List<Picture> pictureList = pictureMapper.selectList(queryWrapper);
        R ok = R.ok(pictureList);
        log.info("ProductServiceImpl.pictures业务结束，结果：{}",ok);
        return ok;
    }


    /**
     * 搜索服务调用，获取全部商品数据进行同步
     *
     * @return 商品数据集合
     */
    @Cacheable(value = "list.category",key = "#root.methodName",cacheManager = "cacheManagerDay")
    @Override
    public List<Product> allList() {

        List<Product> productList = productMapper.selectList(null);
        log.info("ProductServiceImpl.allList业务结束，结果：{}",productList.size());
        return productList;
    }

    /**
     * 搜索业务，需要调用搜索服务
     *
     * @param productSearchParam
     * @return
     */
    @Override
    public R search(ProductSearchParam productSearchParam) {

        R r = searchClient.search(productSearchParam);
        log.info("ProductServiceImpl.search业务结束，结果：{}",r);
        return r;
    }

    /**
     * 根据商品id集合查询商品信息
     *
     * @param productIds
     * @return
     */
    @Cacheable(value = "list.product",key="#productIds")
    @Override
    public R ids(List<Integer> productIds) {

        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        queryWrapper.in("product_id",productIds);

        List<Product> productList = productMapper.selectList(queryWrapper);

        R r = R.ok("类别信息查询成功", productList);
        log.info("ProductServiceImpl.ids业务结束，结果：{}",r);
        return r;
    }

    /**
     * 根据商品id查询商品id集合
     *
     * @param productIds
     * @return
     */
    @Override
    public List<Product> cartList(List<Integer> productIds) {

        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        queryWrapper.in("product_id",productIds);
        List<Product> productList = productMapper.selectList(queryWrapper);
        log.info("ProductServiceImpl.cartList业务结束，结果：{}",productList);
        return productList;
    }

    /**
     * 修改库存和增加销售量
     *
     * @param orderToProducts
     */
    @Override
    public void subNumber(List<OrderToProduct> orderToProducts) {

        Map<Integer, OrderToProduct> map = orderToProducts.stream().collect(Collectors.toMap(OrderToProduct::getProductId, v -> v,(v,v1)->v1));
        Set<Integer> productIds = map.keySet();
        List<Product> productList = productMapper.selectBatchIds(productIds);
        for (Product product : productList) {
            Integer num = map.get(product.getProductId()).getNum();
            product.setProductNum(product.getProductNum()-num);
            product.setProductSales(product.getProductSales()+num);
        }
        this.updateBatchById(productList);
        log.info("ProductServiceImpl.subNumber业务结束，结果：库存和销售量的修改完毕");
    }

    /**
     * 类别对应商品数量查询
     *
     * @param categoryId
     * @return
     */
    @Override
    public Long adminCount(Integer categoryId) {

        QueryWrapper<Product>queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("category_id",categoryId);

        Long count = baseMapper.selectCount(queryWrapper);
        log.info("ProductServiceImpl.adminCount业务结束，结果：{}",count);
        return count;
    }

    /**
     * 商品保存业务
     *
     * 1.商品数据保存
     * 2.图片详情切割和保存
     * 3.搜索数据库的数据添加
     * 4.清空商品相关的缓存数据
     * @param productSaveParam
     * @return
     */
    @CacheEvict(value = "list.product",allEntries = true)
    @Override
    public R adminSave(ProductSaveParam productSaveParam){
        Product product=new Product();
       BeanUtils.copyProperties(productSaveParam,product);
        try {

            int rows = productMapper.insert(product);
            log.info("ProductServiceImpl.adminSave业务结束，结果：{}", rows);
        } catch (Exception e) {
            log.error("插入数据异常", e);
        }

        String pictures = productSaveParam.getPictures();
        if(!StringUtils.isEmpty(pictures)){
            String[] urls = pictures.split("[+]");// 截取特殊字符时 要加\\或者[]

            for (String url : urls) {
                Picture picture=new Picture();
                picture.setProductId(product.getProductId());
                picture.setProductPicture(url);
                pictureMapper.insert(picture);//插入商品图片
            }

        }

        searchClient.saveOrUpdate(product);


        return R.ok("商品数据保存成功");
    }

    /**
     * 商品数据更新
     *1.更新商品数据
     * 2.同步搜索服务数据即可
     * @param product
     * @return
     */
    @Override
    public R adminUpdate(Product product) {

        productMapper.updateById(product);
        searchClient.saveOrUpdate(product);


        return R.ok("商品数据更新成功");
    }

    /**
     * 商品删除业务
     *1.检查购物车
     * 2.检查订单
     * 3.删除商品
     * 4.删除商品相关图片
     * 5.删除收藏
     * 6.进行数据es同步
     * 7.情况缓存
     * @param productId
     * @return
     */

    @Caching(evict = {
            @CacheEvict(value = "list.product",allEntries = true),
            @CacheEvict(value = "product",key = "#productId")
    })
    @Override
    public R adminRemove(Integer productId) {
        R r = cartClient.check(productId);
        if("004".equals(r.getCode())){
            log.info("ProductServiceImpl.adminRemove业务结束，结果：{}", r.getMsg());
            return r;
        }

        r = orderClient.check(productId);
        if("004".equals(r.getCode())){
            log.info("ProductServiceImpl.adminRemove业务结束，结果：{}", r.getMsg());
            return r;
    }

        productMapper.deleteById(productId);
        QueryWrapper<Picture>queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("product_id",productId);
        pictureMapper.delete(queryWrapper);


        collectClient.remove(productId);

        searchClient.remove(productId);
        return R.ok("商品删除成功");
}
}
