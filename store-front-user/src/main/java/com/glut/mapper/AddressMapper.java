package com.glut.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.Address;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressMapper extends BaseMapper<Address> {
}
