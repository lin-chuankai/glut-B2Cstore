package com.glut.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.glut.pojo.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
}
