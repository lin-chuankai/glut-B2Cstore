package com.glut.search.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.glut.doc.ProductDoc;
import com.glut.search.service.SearchService;
import com.glut.param.ProductSearchParam;
import com.glut.pojo.Product;
import com.glut.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/8
 **/
@Service
@Slf4j
public class SearchServiceImpl implements SearchService {


    @Autowired
    private RestHighLevelClient restHighLevelClient;
    /**
     * 根据关键字和分页进行数据查询
     *1.判断关键字是否为空，空查询全部  不为空 all 字段查询
     * 2.添加分页属性
     * 3.es查询
     * 4.结果处理
     * @param productSearchParam
     * @return
     */
    @Override
    public R search(ProductSearchParam productSearchParam) {

        SearchRequest searchRequest=new SearchRequest("product");
        String search = productSearchParam.getSearch();
        if(StringUtils.isEmpty(search)){
            searchRequest.source().query(QueryBuilders.matchAllQuery());
        }else{
            searchRequest.source().query(QueryBuilders.matchQuery("all",search));
        }
        searchRequest.source().from((productSearchParam.getCurrentPage()-1)*productSearchParam.getPageSize());
        searchRequest.source().size(productSearchParam.getPageSize());
        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw  new RuntimeException("查询错误");
        }

        SearchHits hits = searchResponse.getHits();
        long total = hits.getTotalHits().value;

        SearchHit[] hitsHits = hits.getHits();

        List<Product> productList=new ArrayList<>();
        ObjectMapper objectMapper=new ObjectMapper();

        for (SearchHit hitsHit : hitsHits) {
            String sourceAsString = hitsHit.getSourceAsString();

            Product product = null;
            try {
                product = objectMapper.readValue(sourceAsString, Product.class);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            productList.add(product);

        }
        R r = R.ok(null, productList, total);
        log.info("SearchServiceImpl.search业务结束，结果：{}",r);
        return r;
    }

    /**
     * 商品同步 插入和更新
     *
     * @param product
     * @return
     */
    @Override
    public R save(Product product) throws IOException {


        IndexRequest indexRequest=new IndexRequest("product").id(product.getProductId().toString());
        ProductDoc productDoc=new ProductDoc(product);
        ObjectMapper objectMapper=new ObjectMapper();
        String json = objectMapper.writeValueAsString(productDoc);
        indexRequest.source(json, XContentType.JSON);
        restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
        return R.ok("数据同步成功");
    }

    /**
     * 进行es库的商品删除
     *
     * @param productId
     * @return
     */
    @Override
    public R remove(Integer productId) throws IOException {
        DeleteRequest request=new DeleteRequest("product").id(productId.toString());
        restHighLevelClient.delete(request,RequestOptions.DEFAULT);
        return R.ok("es库的数据删除成功");
    }
}
