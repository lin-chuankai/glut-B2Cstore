package com.glut.statics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: TODO
 * @Author: lck
 * @Date: 2023/3/5
 **/
@SpringBootApplication
public class StaticsApplication {

    public static void main(String[] args){

        SpringApplication.run(StaticsApplication.class,args);
    }

}
